# GitLab

## Installation

```shell
git clone https://gitlab.com/jimmy.ljungberg/gitlab-via-docker.git gitlab
```

```shell
docker network create gitlab
```

## Konfiguration

1. ```shell
   cd gitlab
   ```
1. ```shell
   cat <<-EOF >> .env
   EXTERNAL_HOSTNAME=
   GITLAB_DOCKER_IMAGE=gitlab/gitlab-ee:16.0.1-ee.0
   GITLAB_SHELL_SSH_PORT=2222
   GITLAB_REGISTRY_PORT=5050
   EOF
   ```

* `EXTERNAL_HOSTNAME` behöver tilldelas samma hostname/ip-adress som du anger för att nå GitLab-instansen.

GitLab genererar ett lösenord för användaren *root* vid första uppstart och sparar det i filen `/etc/gitlab/initial_root_password`.  
Eftersom `/etc/gitlab` exponeras i denna konfiguration via Docker är det enklast att se innehållet genom att titta i filen `./gitlab/initial_root_password`.

### Administrativ konfiguration

* [First day of the week](https://gitlab.com/help/user/admin_area/settings/index.md#default-first-day-of-the-week)

## Docker

Eftersom denna konfiguration för GitLab inte använder sig av https behöver man tillåta osäker kommunikation mot Docker. Det görs genom att lägga till filen `/etc/docker/daemon.json` på host som ska köra GitLab:

```json
{
   "insecure-registries": [ "<hostname>:<port>" ]
}
```
Där `<hostname>` är samma som `$EXTERNAL_HOSTNAME` och `<port>` är samma som `$GITLAB_REGISTRY_PORT`
